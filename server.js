var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mysql = require("mysql");
var jwt = require("jsonwebtoken");
var crypto = require("crypto");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = 5050;

var router = express.Router();

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'pa-revi',
    password: 'pa-revi-123',
    database: 'proyekakhir'
});

connection.connect(function (err) {
    if (err){
        console.log("Failed to connect MYSQL Database: " + err.stack);
        return;
    }

    console.log("Connected to MYSQL Database as id", connection.threadId);
});

router.post("/login", function (req, res) {
    var data = req.body;

    var hash = crypto.createHash("sha512");

    hash.update(data.password);
    data.password = hash.digest("hex");

    var query = connection.query("SELECT * FROM users WHERE username = " + mysql.escape(data.username) + " AND password = " + mysql.escape(data.password), function (error, results, fields) {
        if (error)
            throw error;

        var queryResults = JSON.stringify(results);

        console.log(queryResults);

        if (JSON.parse(queryResults).length !== 0){
            var userData = JSON.parse(queryResults);

            delete userData[0].password;

            var token = jwt.sign(userData[0], "ProyekAkhir", {expiresIn: '50000h'});

            var response = {
                code: 200,
                token: token,
                data: JSON.stringify(userData[0])
            };

            res.json(response);
        } else {
            res.status(404).end();
        }
    });

    console.log(query.sql);
});

router.post("/register", function (req, res) {
    var data = req.body;

    var hash = crypto.createHash("sha512");

    hash.update(data.password);
    data.password = hash.digest("hex");

    var query = connection.query("INSERT INTO users SET ?", data, function (error, results, fields) {
        if (error)
            throw error;

        delete data.password;

        data.id = results.insertId;

        res.json(data);
    });

    console.log(query.sql);
});

app.use("/api", router);

app.listen(port);
console.log("Server is running on port", port);